module.exports = ({ env }) => ({
  // enable a plugin that doesn't require any configuration
  i18n: true,

  graphql: {
    enabled: true,
    config: {
      playgroundAlways: true,
      shadowCRUD: true,
      generateArtifacts: true,
      artifacts: {
        schema: "/opt/app/generated/schema",
        typegen: "/opt/app/generated/type"
      },
      apolloServer: {
        tracing: false,
      },
    },
  },
});
